const Promise = require('bluebird');
const _ = require('lodash');

const Errors = require('eratum');

function stringify(...args) {
	return args.map(item => require('util').inspect(item, { showHidden: false, depth: null })).join(', ');
}

function verify(val, verifying) {
	if (typeof verifying === 'function') {
		return verifying(val);
	} if (Array.isArray(verifying) && verifying.length) {
		const [ logic, ...array ] = verifying;

		return array.map(test => verify(val, test)).reduce(logic);
	}

	return true;
}

/**
 * Access 'constructor.name'.<br/>
 * if undefined or null, return 'undefined'
 *
 * @param  {*} data argument
 * @returns {String} Type name, one of 'undefined', 'Boolean', 'Number', 'String', 'Array', 'Object', 'Function', '${ClassName}'
 */
function instanceOf(data) {
	return (data !== undefined && data !== null) ? data.constructor.name : 'undefined';
}

/**
 * Compare if item is an element of type named by the string expectedType.
 *
 * @param  {*} item Element for which type is checked.
 * @param  {String} expectedInstance String representation of type looking to
 * @return {Boolean} true if data or its ancestry is type of expectedType.
 * @see {instanceOf}
 */
function isInstance(item, expectedInstance) {
	if (item === undefined || item === null) {
		return expectedInstance === 'undefined';
	}

	const isFinalInstance = [ 'Boolean', 'Number', 'String', 'Array', 'Object', 'Function' ];
	const proto = Object.getPrototypeOf(item);
	const type = instanceOf(proto);

	return (type === expectedInstance) || (!isFinalInstance.includes(type) && isInstance(proto, expectedInstance));
}

function validate(object, key, options = { }) {
	if (object[key] === undefined) {
		if (typeof options.default !== 'undefined') {
			object[key] = options.default;
			validate(object, key, { ...options, default: undefined });
		} else if (options.required) {
			throw Errors.doesntExist({ name: key });
		}
	} else if (object[key] === null) {
		if (typeof options.default !== 'undefined') {
			object[key] = options.default;
			validate(object, key, { ...options, default: undefined });
		} else if (!options.allowNull) {
			if (options.required) {
				throw Errors.doesntExist({ name: key });
			}
			if (options.allowNull === false) {
				delete object[key];
			}
		}
	} else {
		if (typeof options.convert === 'function') {
			object[key] = options.convert(object[key]);
		}

		if (typeof options.instance === 'string') {
			new Validator(object[key], key).instance(options.instance).try();
		}
		if (!verify(object[key], options.verify)) {
			throw Errors.invalid({ name: key, reason: 'Parameters doesn\'t validate control function.' });
		}
	}
}

class Validator {
	constructor(value, name = '') {
		this.value = value;
		this.name = name;
		this.modifier = true;
		this.children = [ ];
		this._isValid = true;
		this.error = null;

		/* key words */
		this.must = this;
		this.have = this;
		this.has = this;
		this.been = this;
		this.be = this;
		this.is = this;
		this.are = this;
		this.a = this;
		this.an = this;
		this.and = this;
		this.of = this;
	}

	static create(...args) {
		return new Validator(...args);
	}

	invalidate(error) {
		this._isValid = false;
		this.error = error;

		return this;
	}

	resetModifier() {
		this.modifier = true;

		return this;
	}

	get isValid() {
		return this._isValid && this.children.map(child => child.isValid).reduce((a, b) => a && b, true);
	}

	get not() {
		this.modifier = !this.modifier;

		return this;
	}

	invalidateOn(test, buildError, buildRevError) {
		if (this.isValid === true) {
			try {
				if (test() === this.modifier) {
					this.invalidate(this.modifier ? buildError() : buildRevError());
				}
			} catch (cause) {
				throw Errors.programingFault({ reason: 'Validation was interupted by unhandeled throws error', cause });
			}
		}

		return this.resetModifier();
	}

	exist() {
		return this.invalidateOn(() => instanceOf(this.value) === 'undefined',
			() => Errors.doesntExist({ name: this.name }),
			() => Errors.exist({ name: this.name }));
	}

	type(expectedType) {
		const actualType = typeof this.value;

		return this.invalidateOn(() => actualType !== expectedType,
			() => Errors.invalidType({ name: this.name, actualType, expectedType: `${expectedType}` }),
			() => Errors.invalidType({ name: this.name, actualType, expectedType: `!${expectedType}` }));
	}

	instance(...args) {
		const expectedTypes = _.flattenDeep(args);

		return this.invalidateOn(() => expectedTypes.map(expectedType => !isInstance(this.value, expectedType)).reduce((a, b) => a && b),
			() => Errors.invalidType({ name: this.name, actualType: instanceOf(this.value), expectedType: `${expectedTypes.join(', ')}` }),
			() => Errors.invalidType({ name: this.name, actualType: instanceOf(this.value), expectedType: `!${expectedTypes.join(', ')}` }));
	}

	equal(value) {
		return this.invalidateOn(() => !_.isEqual(this.value, value),
			() => Errors.notEqual({ name: this.name, actualValue: stringify(this.value), expectedValue: stringify(value) }),
			() => Errors.equal({ name: this.name, value: stringify(this.value) }));
	}

	among(values) {
		return this.invalidateOn(() => typeof values.find(value => _.isEqual(this.value, value)) === 'undefined',
			() => Errors.notIncluded({ name: this.name, value: stringify(this.value), possibleValues: stringify(values) }),
			() => Errors.included({ name: this.name, value: stringify(this.value), forbiddenValues: stringify(values) }));
	}

	greaterThan(limit) {
		return this.invalidateOn(() => this.value <= limit,
			() => Errors.notGreaterThan({ name: this.name, value: stringify(this.value), limit: stringify(limit) }),
			() => Errors.greaterThan({ name: this.name, value: stringify(this.value), limit: stringify(limit) }));
	}

	lowerThan(limit) {
		return this.invalidateOn(() => this.value >= limit,
			() => Errors.notLowerThan({ name: this.name, value: stringify(this.value), limit: stringify(limit) }),
			() => Errors.lowerThan({ name: this.name, value: stringify(this.value), limit: stringify(limit) }));
	}

	match(regex) {
		return this.invalidateOn(() => !regex.test(this.value),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: `regex(${stringify(regex)})` }),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: `!regex(${stringify(regex)})` }));
	}

	startsWith(needle) {
		return this.invalidateOn(() => !this.value.startsWith(needle),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: `startsWith(${stringify(needle)})` }),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: `!startsWith(${stringify(needle)})` }));
	}

	endsWith(needle) {
		return this.invalidateOn(() => !this.value.endsWith(needle),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: `endsWith(${stringify(needle)})` }),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: `!endsWith(${stringify(needle)})` }));
	}

	hexadecimal() {
		return this.invalidateOn(() => !/^([0-9a-fA-F]{2})*$/.test(this.value),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: 'hexadecimal' }),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: '!hexadecimal' }));
	}

	base64() {
		return this.invalidateOn(() => !/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{2,3}==?)?$/.test(this.value),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: 'base64' }),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: '!base64' }));
	}

	url() {
		return this.invalidateOn(() => !RegExp('^(http://www.|https://www.|http://|https://)?[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$').test(this.value),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: 'url' }),
			() => Errors.invalidFormat({ name: this.name, value: stringify(this.value), format: '!url' }));
	}

	boolean() {
		return this.instance('Boolean');
	}

	number() {
		return this.instance('Number');
	}

	string() {
		return this.instance('String');
	}

	object() {
		return this.instance('Object');
	}

	array() {
		return this.invalidateOn(() => !Array.isArray(this.value),
			() => Errors.invalidType({ name: this.name, actualType: instanceOf(this.value), expectedType: 'Array' }),
			() => Errors.invalidType({ name: this.name, actualType: instanceOf(this.value), expectedType: '!Array' }));
	}

	length(length) {
		return this.invalidateOn(() => this.value.length !== length,
			() => Errors.notEqual({ name: `${this.name}.length`, actualValue: this.value.length, expectedValue: length }),
			() => Errors.equal({ name: `${this.name}.length`, value: this.value.length }));
	}

	lengthGreaterThan(limit) {
		console.warn('lengthGreaterThan is depreciated. Use keys instead');

		return this.invalidateOn(() => this.value.length <= limit,
			() => Errors.notGreaterThan({ name: this.name, value: this.value, limit }),
			() => Errors.greaterThan({ name: this.name, value: this.value, limit }));
	}

	lengthLowerThan(limit) {
		console.warn('lengthLowerThan is depreciated. Use keys instead');

		return this.invalidateOn(() => this.value.length >= limit,
			() => Errors.notLowerThan({ name: `${this.name}.length`, value: this.value.length, limit }),
			() => Errors.lowerThan({ name: `${this.name}.length`, value: this.value.length, limit }));
	}

	each(apply) {
		try {
			if (this.isValid === true) {
				const children = this.value.map((child, idx) => new Validator(child, `${this.name}[${idx}]`));
				this.children.push(...children);
				children.forEach(apply);
			}
		} catch (cause) {
			this.invalidate(Errors.programingFault({ reason: 'Children validation interupted by unhandeled throws error', cause }));
		}

		return this;
	}

	property(key) {
		console.warn('Property is depreciated and full of bugs. Use keys instead');
		const child = new Validator(this.value[key], `${this.name}.${key}`);
		this.children.push(child);

		child.modifier = this.modifier;
		this.modifier = true;

		return child.exist();
	}

	keys(...args) {
		const parameterToValidator = (parameter, index) => {
			const parameterName = `parameter${index || ''}`;
			try {
				const parameterValidator = Validator.create(parameter, parameterName);
				const actualType = instanceOf(parameter);
				switch (actualType) {
				case 'Array':
					return parameter.map(parameterToValidator);
				case 'Object':
					parameterValidator.keys('key', vKey => vKey.string()).try();

					return parameter.optional && instanceOf(this.value[parameter.key]) === 'undefined' ? null : new Validator(this.value[parameter.key], `${this.name}.${parameter.key}`).exist();
				case 'String':
					return new Validator(this.value[parameter], `${this.name}.${parameter}`).exist();
				default:
					throw Errors.invalidType({ name: parameterName, actualType, expectedType: 'Array|String|Object' });
				}
			} catch (cause) {
				throw cause instanceof Errors && cause.code === Errors.INVALID_TYPE ? cause : Errors.invalid({ name: parameterName, cause });
			}
		};

		try {
			if (this.isValid === true) {
				const [ [ apply ], parameters ] = _.partition(args, arg => isInstance(arg, 'Function'));
				const children = parameters.map(parameterToValidator);
				this.children.push(..._.compact(_.flattenDeep(children)));
				if (this.isValid === true && apply) {
					apply(...children);
				}
			}
		} catch (cause) {
			this.invalidate(Errors.programingFault({ reason: 'Children validation was interupted by unhandeled throws error', cause }));
		}

		return this;
	}

	try() {
		if (this._isValid !== true) {
			throw this.error;
		}
		try {
			this.children.forEach(child => child.try());
		} catch (cause) {
			throw Errors.invalid({ name: this.name, cause });
		}

		return this;
	}

	resolve() {
		return Promise.try(() => this.try());
	}
}

const newValidator = (...args) => new Validator(...args);

newValidator.instanceOf = instanceOf;
newValidator.isInstance = isInstance;
newValidator.validate = validate;
newValidator.controlBody = (key, options) => (req, res, next) => {
	try {
		validate(req.body, key, options);

		return next();
	} catch (error) {
		return res.status(400).json((error && error.get && error.get()) || Errors.programingFault({ reason: 'Unhandeling rejection', cause: error }));
	}
};

module.exports = newValidator;
