require('assert');
const should = require('should');

const Errors = require('eratum');

Errors.isStackEnabled = true;

const Validator = require('../index');

describe('instanceOf', () => {
	it('Validator.instanceOf() should return \'undefined\'', () => {
		Validator.instanceOf().should.equals('undefined');
	});
	it('Validator.instanceOf(null) should return \'undefined\'', () => {
		Validator.instanceOf(null).should.equals('undefined');
	});
	it('Validator.isInstance(null, \'undefined\') should return true', () => {
		Validator.isInstance(null, 'undefined').should.equals(true);
	});
	it('Validator.isInstance(undefined, \'undefined\') should return true', () => {
		Validator.isInstance(undefined, 'undefined').should.equals(true);
	});
	it('Validator.isInstance({}, \'Object\') should return true', () => {
		Validator.isInstance({}, 'Object').should.equals(true);
	});
	it('Validator.isInstance(new Error(), \'Object\') should return true', () => {
		Validator.isInstance(new Error(), 'Object').should.equals(true);
	});
	it('Validator.isInstance(14, \'Object\') should return false', () => {
		Validator.isInstance(14, 'Object').should.equals(false);
	});
	it('Validator.isInstance({}, \'undefined\') should return false', () => {
		Validator.isInstance({}, 'undefined').should.equals(false);
	});
});

describe('Chain', () => {
	it('Chain should return this', () => {
		const v = Validator();
		v.must.have.has.been.be.is.are.a.an.and.of.should.equal(v);
	});
	it('Validator(null).not.exist().isValid should be true', () => {
		Validator(null).not.exist().isValid.should.equal(true);
	});
	it('Validator(17).not.exist().isValid should be false', () => {
		Validator(17).not.exist().isValid.should.equal(false);
	});
	it('Validator(null).exist().exist().isValid should be false', () => {
		Validator(null).exist().exist().isValid.should.equal(false);
	});
	it('Validator(null).not.exist().exist().isValid should be false', () => {
		Validator(null).not.exist().exist().isValid.should.equal(false);
	});
	it('Validator(null).length(0) should throw PROGRAMING_FAULT', () => {
		try {
			Validator(null).length(0);
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.PROGRAMING_FAULT);
		}
	});
});

describe('Rejections', () => {
	it('Shoud throw error on invalidated validator', () => {
		const error = Errors.internalError();
		try {
			Validator().invalidate(error).try();
			should.fail(undefined, undefined, 'Should throw');
		} catch (e) {
			e.should.equal(error);
		}
	});
	it('Shoud reject error on invalidated validator', () => {
		const error = Errors.internalError();

		return Validator().invalidate(error).resolve().should.Promise().rejectedWith(error);
	});
});

describe('Exist', () => {
	it('Validator().exist() should be invalid', () => {
		const v = Validator().exist();
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.DOESNT_EXIST);
	});
	it('Validator(null).exist() should be invalid', () => {
		const v = Validator(null).exist();
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.DOESNT_EXIST);
	});
	it('Validator(17).exist() should be valid', () => {
		Validator(17).exist().isValid.should.equal(true);
	});
	it('Validator([]).exist() should be valid', () => {
		Validator([]).exist().isValid.should.equal(true);
	});
	it('Validator({}).exist() should be valid', () => {
		Validator({}).exist().isValid.should.equal(true);
	});
});

describe('Type', () => {
	it('Validator(42).type(\'number\') should be valid', () => {
		const v = Validator(42).type('number');
		v.isValid.should.equal(true);
	});
	it('Validator(true).type(\'string\') should be invalid', () => {
		const v = Validator(true).type('string');
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
	it('Validator([]).not.type(\'function\') should be valid', () => {
		const v = Validator([]).not.type('function');
		v.isValid.should.equal(true);
	});
	it('Validator(new Error()).not.type(\'object\') should be invalid', () => {
		const v = Validator(new Error()).not.type('object');
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
});

describe('Instance', () => {
	it('Validator(42).instance(\'Number\') should be valid', () => {
		const v = Validator(42).instance('Number');
		v.isValid.should.equal(true);
	});
	it('Validator(true).instance(\'String\') should be invalid', () => {
		const v = Validator(true).instance('String');
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
	it('Validator([]).not.instance(\'Object\') should be valid', () => {
		const v = Validator([]).not.instance('Object');
		v.isValid.should.equal(true);
	});
	it('Validator(new Error()).not.instance(\'Error\') should be invalid', () => {
		const v = Validator(new Error()).not.instance('Error');
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
	it('Validator(13).instance(\'Error\', \'Number\') should be valid', () => {
		const v = Validator(13).instance('Error', 'Number');
		v.isValid.should.equal(true);
	});
	it('Validator(13).instance([\'Number\', \'String\']) should be valid', () => {
		const v = Validator(13).instance([ 'Number', 'String' ]);
		v.isValid.should.equal(true);
	});
	it('Validator(13).instance(\'Error\', \'String\') should be invalid', () => {
		const v = Validator(13).instance('Error', 'String');
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
	it('Validator(13).instance([\'Error\', \'String\']) should be invalid', () => {
		const v = Validator(13).instance([ 'Error', 'String' ]);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
});

describe('Equal', () => {
	it('Validator(42).equal(42) should be valid', () => {
		const v = Validator(42).equal(42);
		v.isValid.should.equal(true);
	});
	it('Validator(true).equal(\'String\') should be invalid', () => {
		const v = Validator(true).equal('String');
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_EQUAL);
	});
	it('Validator([]).not.equal(\'Object\') should be valid', () => {
		const v = Validator([]).not.equal('Object');
		v.isValid.should.equal(true);
	});
	it('Validator({ a: 12 }).not.equal({ a: 12 }) should be invalid', () => {
		const v = Validator({ a: 12 }).not.equal({ a: 12 });
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.EQUAL);
	});
});

describe('Among', () => {
	it('Validator(42).among([ 42, 72 ]) should be valid', () => {
		const v = Validator(42).among([ 42, 72 ]);
		v.isValid.should.equal(true);
	});
	it('Validator(true).among([ false, { a: 12 } ]) should be invalid', () => {
		const v = Validator(true).among([ false, { a: 12 } ]);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_INCLUDED);
	});
	it('Validator(27).not.among([ false, { a: 12 } ]) should be valid', () => {
		const v = Validator([]).not.among([ false, { a: 12 } ]);
		v.isValid.should.equal(true);
	});
	it('Validator({ a: 12 }).not.among([ { a: 12 } ]) should be invalid', () => {
		const v = Validator({ a: 12 }).not.among([ { a: 12 } ]);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INCLUDED);
	});
});

describe('GreaterThan', () => {
	it('Validator(42).greaterThan(28) should be valid', () => {
		const v = Validator(42).greaterThan(28);
		v.isValid.should.equal(true);
	});
	it('Validator(42).greaterThan(57) should be invalid', () => {
		const v = Validator(42).greaterThan(57);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_GREATER_THAN);
	});
	it('Validator(42).not.greaterThan(57) should be valid', () => {
		const v = Validator(42).not.greaterThan(57);
		v.isValid.should.equal(true);
	});
	it('Validator(42).not.greaterThan(28) should be invalid', () => {
		const v = Validator(42).not.greaterThan(28);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.GREATER_THAN);
	});
});

describe('LowerThan', () => {
	it('Validator(42).lowerThan(57) should be valid', () => {
		const v = Validator(42).lowerThan(57);
		v.isValid.should.equal(true);
	});
	it('Validator(42).lowerThan(28) should be invalid', () => {
		const v = Validator(42).lowerThan(28);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_LOWER_THAN);
	});
	it('Validator(42).not.lowerThan(28) should be valid', () => {
		const v = Validator(42).not.lowerThan(28);
		v.isValid.should.equal(true);
	});
	it('Validator(42).not.lowerThan(57) should be invalid', () => {
		const v = Validator(42).not.lowerThan(57);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.LOWER_THAN);
	});
});

describe('Match', () => {
	it('Validator(\'Hello World\').match(/^[ \\w]{11}$/) should be valid', () => {
		return Validator('Hello World').match(/^[ \w]{11}$/).resolve()
			.should.resolved();
	});
	it('Validator(\'Hello World\').match(/[0-9]\\+/) should be invalid', () => {
		return Validator('Hello World', 'ISKVRZLVNB').match(/[0-9]\+/).match(/^[ \w]{11}$/).resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
	it('Validator(\'Hello World\').not.match(/[0-9]\\+/) should be valid', () => {
		return Validator('Hello World').not.match(/[0-9]\+/).resolve()
			.should.resolved();
	});
	it('Validator(\'Hello World\').not.match(/^[ \\w]{11}$/) should be invalid', () => {
		return Validator('Hello World', 'ISKVRZLVNB').not.match(/^[ \w]{11}$/).resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
});

describe('Hexadecimal', () => {
	it('Validator(\'0123456789abcdefABCDEF\').hexadecimal() should be valid', () => {
		return Validator('0123456789abcdefABCDEF').hexadecimal().resolve()
			.should.resolved();
	});
	it('Validator(\'0123456789 abcdef ABCDEF\').hexadecimal() should be invalid', () => {
		return Validator('0123456789 abcdef ABCDEF', 'ISKVRZLVNB').hexadecimal().resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
	it('Validator(\'0123456789 abcdef ABCDEF\').not.hexadecimal() should be valid', () => {
		return Validator('0123456789 abcdef ABCDEF').not.hexadecimal().resolve()
			.should.resolved();
	});
	it('Validator(\'0123456789abcdefABCDEF\').not.hexadecimal() should be invalid', () => {
		return Validator('0123456789abcdefABCDEF', 'ISKVRZLVNB').not.hexadecimal().resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
});

describe('Base64', () => {
	it('Validator(\'0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN=\').base64() should be valid', () => {
		return Validator('0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN=').base64().resolve()
			.should.resolved();
	});
	it('Validator(\'132\').base64() should be invalid', () => {
		return Validator('132', 'ISKVRZLVNB').base64().resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
	it('Validator(\'132\').not.base64() should be valid', () => {
		return Validator('0123456789 abcdef ABCDEF').not.base64().resolve()
			.should.resolved();
	});
	it('Validator(\'0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN=\').not.base64() should be invalid', () => {
		return Validator('0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN=', 'ISKVRZLVNB').not.base64().resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
});

describe('URL', () => {
	it('Validator(\'www.google.com\').url() should be valid', () => {
		return Validator('www.google.com').url().resolve()
			.should.resolved();
	});
	it('Validator(\'www.goog le.com\').url() should be invalid', () => {
		return Validator('www.goog le.com', 'ISKVRZLVNB').url().resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
	it('Validator(\'www.go=ogle.com\').not.url() should be valid', () => {
		return Validator('www.go=ogle.com').not.url().resolve()
			.should.resolved();
	});
	it('Validator(\'www.google.com\').not.url() should be invalid', () => {
		return Validator('www.google.com', 'ISKVRZLVNB').not.url().resolve()
			.should.rejectedWith(Errors, {
				code: Errors.INVALID_FORMAT,
				parameters: {
					name: 'ISKVRZLVNB',
				},
			});
	});
});

describe('Array', () => {
	it('Validator([ ]).array() should be valid', () => {
		const v = Validator([ ]).array();
		v.isValid.should.equal(true);
	});
	it('Validator({ }).array() should be invalid', () => {
		const v = Validator({ }).array();
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
	it('Validator({ }).not.array() should be valid', () => {
		const v = Validator({ }).not.array();
		v.isValid.should.equal(true);
	});
	it('Validator([ ]).not.base64() should be invalid', () => {
		const v = Validator([ ]).not.array();
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.INVALID_TYPE);
	});
});

describe('Length', () => {
	it('Validator([ ]).length(0) should be valid', () => {
		const v = Validator([ ]).length(0);
		v.isValid.should.equal(true);
	});
	it('Validator([ 1, 2, 3 ]).length(0) should be invalid', () => {
		const v = Validator([ 1, 2, 3 ]).length(0);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_EQUAL);
	});
	it('Validator([ ]).not.length(3) should be valid', () => {
		const v = Validator([ ]).not.length(3);
		v.isValid.should.equal(true);
	});
	it('Validator([ 1, 2, 3 ]).not.length(3) should be invalid', () => {
		const v = Validator([ 1, 2, 3 ]).not.length(3);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.EQUAL);
	});
});

describe('LengthGreaterThan', () => {
	it('Validator([ 1, 2, 3 ]).lengthGreaterThan(0) should be valid', () => {
		const v = Validator([ 1, 2, 3 ]).lengthGreaterThan(0);
		v.isValid.should.equal(true);
	});
	it('Validator([ 1, 2, 3 ]).lengthGreaterThan(3) should be invalid', () => {
		const v = Validator([ 1, 2, 3 ]).lengthGreaterThan(3);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_GREATER_THAN);
	});
	it('Validator([ 1, 2, 3 ]).not.lengthGreaterThan(3) should be valid', () => {
		const v = Validator([ 1, 2, 3 ]).not.lengthGreaterThan(3);
		v.isValid.should.equal(true);
	});
	it('Validator([ 1, 2, 3 ]).not.lengthGreaterThan(0) should be invalid', () => {
		const v = Validator([ 1, 2, 3 ]).not.lengthGreaterThan(0);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.GREATER_THAN);
	});
});

describe('LengthLowerThan', () => {
	it('Validator([ 1, 2, 3 ]).lengthLowerThan(5) should be valid', () => {
		const v = Validator([ 1, 2, 3 ]).lengthLowerThan(5);
		v.isValid.should.equal(true);
	});
	it('Validator([ 1, 2, 3 ]).lengthLowerThan(0) should be invalid', () => {
		const v = Validator([ 1, 2, 3 ]).lengthLowerThan(0);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.NOT_LOWER_THAN);
	});
	it('Validator([ 1, 2, 3 ]).not.lengthLowerThan(3) should be valid', () => {
		const v = Validator([ 1, 2, 3 ]).not.lengthLowerThan(3);
		v.isValid.should.equal(true);
	});
	it('Validator([ 1, 2, 3 ]).not.lengthLowerThan(5) should be invalid', () => {
		const v = Validator([ 1, 2, 3 ]).not.lengthLowerThan(5);
		v.isValid.should.equal(false);
		v.error.should.have.property('code').equal(Errors.LOWER_THAN);
	});
});

describe('Property', () => {
	it('Validator({ a: property }).property(\'a\') return validator on \'a\' property', () => {
		const parent = Validator({ a: 12 });
		const child = parent.property('a').not.exist();
		child.value.should.equal(12);
	});
	it('Should invalidate parent on property invalidation', () => {
		const parent = Validator({ a: 12 });
		const child = parent.property('a').not.exist();
		child.isValid.should.equal(false);
		child.error.should.have.property('code').equal(Errors.EXIST);
		parent.isValid.should.equal(false);
		should(parent.error).equal(null);
	});
	it('Should forward modifier to property and reset parent', () => {
		const parent = Validator({ a: 12 });
		const child = parent.not.property('a');
		child.isValid.should.equal(false);
		child.error.should.have.property('code').equal(Errors.EXIST);
		parent.modifier.should.equal(true);
	});
});

describe('Keys', () => {
	it('Should allow chain on invalid validator', () => {
		const parent = Validator(null).object().keys('test');
		parent.isValid.should.equal(false);
	});
	it('Should not validate on invalid type parameter', () => {
		try {
			Validator({ }).keys(47).try();
			should.fail(null, null, 'Should throws');
		} catch (error) {
			error.should.property('code').equals(Errors.PROGRAMING_FAULT);
			error.should.property('tag').equals('PROGRAMING_FAULT');
			error.should.property('cause').type('object');
			error.cause.should.property('code').equals(Errors.INVALID_TYPE);
			error.cause.should.property('tag').equals('INVALID_TYPE');
			error.cause.should.property('parameters').type('object').property('actualType').equals('Number');
		}
	});
	it('Should validate single property label', () => {
		try {
			Validator({ a: 12 }).keys('a').try();
		} catch (error) {
			should.fail(null, null, `Should not throws ${error}`);
		}
	});
	it('Should not validate single property label', () => {
		try {
			Validator({ }, 'qzreshtjdu').keys('a').try();
			should.fail(null, null, 'Should throws');
		} catch (error) {
			error.should.property('code').equals(Errors.INVALID);
			error.should.property('tag').equals('INVALID');
			error.should.property('parameters').type('object').property('name').equals('qzreshtjdu');
			error.should.property('cause').type('object');
			error.cause.should.property('code').equals(Errors.DOESNT_EXIST);
			error.cause.should.property('tag').equals('DOESNT_EXIST');
			error.cause.should.property('parameters').type('object').property('name').equals('qzreshtjdu.a');
		}
	});
	it('Should validate single property label', () => {
		try {
			Validator({ id: 12 }, 'rqghsryj').keys('id', vId => vId.number().greaterThan(0).lowerThan(10)).try();
			should.fail(null, null, 'Should throws 12 greater than 10');
		} catch (error) {
			error.should.property('code').equals(Errors.INVALID);
			error.should.property('tag').equals('INVALID');
			error.should.property('parameters').type('object').property('name').equals('rqghsryj');
			error.should.property('cause').type('object');
			error.cause.should.property('code').equals(Errors.NOT_LOWER_THAN);
			error.cause.should.property('tag').equals('NOT_LOWER_THAN');
			error.cause.should.property('parameters').type('object');
			error.cause.parameters.should.property('name').equals('rqghsryj.id');
			error.cause.parameters.should.property('value').equals('12');
			error.cause.parameters.should.property('limit').equals('10');
		}
	});
	it('Should validate multiple properties', () => {
		try {
			const obj = { id: 12, name: 'john', data: { amount: 27, enabled: true } };
			Validator(obj, 'obj').keys('id', 'name', 'data', (vId, vName, vData) => {
				vId.number().not.equal(0);
				vName.string().startsWith('j');
				vData.object().keys('amount', 'enabled', (vAmount, vEnabled) => {
					vAmount.number();
					vEnabled.boolean();
				});
			}).try();
		} catch (error) {
			should.fail(null, null, `Should not throws ${error}`);
		}
	});
	it('Should not validate multiple properties', () => {
		try {
			const obj = { id: 12, name: 'john', data: { amount: 27, enabled: true } };
			Validator(obj, 'obj').keys('id', 'name', 'data', (vId, vName, vData) => {
				vId.number().not.equal(0);
				vName.string().startsWith('j');
				vData.object().keys('amount', 'enabled', (vAmount, vEnabled) => {
					vAmount.string();
					vEnabled.boolean();
				});
			}).try();
		} catch (error) {
			error.should.property('code').equals(Errors.INVALID);
			error.should.property('tag').equals('INVALID');
			error.should.property('cause').type('object');
			error.cause.should.property('code').equals(Errors.INVALID);
			error.cause.should.property('tag').equals('INVALID');
			error.cause.should.property('cause').type('object');
			error.cause.cause.should.property('code').equals(Errors.INVALID_TYPE);
			error.cause.cause.should.property('tag').equals('INVALID_TYPE');
		}
	});
	it('Should validate array properties', () => {
		try {
			const obj = { id: 12, name: 'john', data: { amount: 27, enabled: true } };
			Validator(obj, 'obj').keys([ 'id', 'name', 'data' ], ([ vId, vName, vData ]) => {
				vId.number().not.equal(0);
				vName.string().startsWith('j');
				vData.object().keys([ 'amount', 'enabled' ], ([ vAmount, vEnabled ]) => {
					vAmount.number();
					vEnabled.boolean();
				});
			}).try();
		} catch (error) {
			should.fail(null, null, `Should not throws ${error}`);
		}
	});
	it('Should validate mixed properties', () => {
		try {
			const obj = { id: 12, name: 'john', data: { amount: 27, enabled: true } };
			Validator(obj, 'obj').keys([ 'id' ], [ 'name', [ 'data' ] ], ([ vId ], [ vName, [ vData ] ]) => {
				vId.number().not.equal(0);
				vName.string().startsWith('j');
				vData.object().keys([ [ 'amount' ] ], 'enabled', ([ [ vAmount ] ], vEnabled) => {
					vAmount.number();
					vEnabled.boolean();
				});
			}).try();
		} catch (error) {
			should.fail(null, null, `Should not throws ${error}`);
		}
	});
	it('Should not validate on invalid object parameter', () => {
		const parent = Validator({ }).keys({ not_key: 'test' });
		parent.isValid.should.equal(false);
	});
	it('Should not validate on invalid object parameter', () => {
		const parent = Validator({ }).keys({ key: 74 });
		parent.isValid.should.equal(false);
	});
	it('Should validate single property object', () => {
		const parent = Validator({ a: 12 }).keys({ key: 'a' });
		parent.isValid.should.equal(true);
	});
	it('Should validate optional property object', () => {
		const parent = Validator({ }).keys({ key: 'a', optional: true });
		parent.isValid.should.equal(true);
	});
	it('Should validate full mixed properties', () => {
		try {
			const obj = { id: 12, name: 'john', data: { amount: 27, enabled: true } };
			Validator(obj, 'obj').keys([ 'id' ], [ { key: 'name' }, [ 'data' ] ], { key: 'error', optional: true }, ([ vId ], [ vName, [ vData ] ], vError) => {
				vId.number().not.equal(0);
				vName.string().startsWith('j');
				vData.object().keys([ [ { key: 'amount', optional: true } ] ], 'enabled', ([ [ vAmount ] ], vEnabled) => {
					if (vAmount) {
						vAmount.number();
					}
					vEnabled.boolean();
				});
				if (vError) {
					vError.instance('Errors');
				}
			}).try();
		} catch (error) {
			should.fail(null, null, `Should not throws ${error}`);
		}
	});

	// TODO throwing apply function
	// TODO invalid params inside Chain
});

describe('Each', () => {
	it('Should allow chain on invalid validator', () => {
		const parent = Validator(12).array().each(child => child.instance('Number'));
		parent.isValid.should.equal(false);
	});
	it('Should validate on each array item', () => {
		const parent = Validator([ 1, 2, 3 ])
			.array().each(child => child.instance('Number'));
		parent.isValid.should.equal(true);
	});
	it('Should not validate on each array item', () => {
		const parent = Validator([ 1, false, 3 ])
			.array().each(child => child.instance('Number'));
		parent.isValid.should.equal(false);
	});
	// TODO throwing apply function
});

describe('Try / Resolve', () => {
	it('Should continue on try valid validator', () => {
		try {
			Validator([ 1, 2, 3 ]).instance('Array').try();
		} catch (error) {
			should(error).equal(null);
		}
	});
	it('Should continue on try valid neasted validator', () => {
		try {
			Validator([ 1, false, 3 ], 'array')
				.array().each(child => child.instance('Number')).try();
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.property('code').equal(Errors.INVALID);
			error.should.property('cause');
			error.cause.should.property('code').equal(Errors.INVALID_TYPE);
		}
	});
	it('Should throw error on try invalid validator', () => {
		try {
			Validator([ 1, 2, 3 ]).type('Number').try();
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.property('code').equal(Errors.INVALID_TYPE);
		}
	});
	it('Should throw error on try invalid neasted validator', () => {
		try {
			Validator([ 1, false, 3 ], 'array')
				.array().each(child => child.instance('Number')).try();
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.property('code').equal(Errors.INVALID);
			error.should.property('cause');
			error.cause.should.property('code').equal(Errors.INVALID_TYPE);
		}
	});
	it('Should fullfil on resolve valid validator', () => {
		return Validator([ 1, 2, 3 ]).instance('Array').resolve().should.fulfilled();
	});
	it('Should reject error on resolve invalid validator', () => {
		return Validator([ 1, 2, 3 ]).instance('Number').resolve().should.rejected();
		// error.should.property('code').equal(Errors.INVALID_TYPE);
	});
});


describe('Validate', () => {
	it('Should validate undefined no options', () => {
		try {
			const object = { };
			Validator.validate(object, 'a');
		} catch (error) {
			should(error).equal(null);
		}
	});
	it('Should not validate undefined required', () => {
		try {
			const object = { };
			Validator.validate(object, 'a', { required: true });
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.DOESNT_EXIST);
		}
	});
	it('Should not validate undefined default null required', () => {
		try {
			const object = { };
			const def = null;
			Validator.validate(object, 'a', { required: true, default: def });
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.DOESNT_EXIST);
		}
	});
	it('Should validate undefined default null required allow null', () => {
		try {
			const object = { };
			const def = null;
			Validator.validate(object, 'a', { required: true, allowNull: true, default: def });
			object.should.property('a').equal(def);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate null no options', () => {
		try {
			const object = { a: null };
			Validator.validate(object, 'a');
			object.should.property('a').equal(null);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate null not required but not null', () => {
		try {
			const object = { a: null };
			Validator.validate(object, 'a', { allowNull: false });
			object.should.not.property('a');
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should not validate null required', () => {
		try {
			const object = { a: null };
			Validator.validate(object, 'a', { required: true });
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.DOESNT_EXIST);
		}
	});
	it('Should validate null default 8 required', () => {
		try {
			const object = { a: null };
			Validator.validate(object, 'a', { required: true, default: 8 });
			object.should.property('a').equal(8);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate null required allowNull', () => {
		try {
			const object = { a: null };
			Validator.validate(object, 'a', { required: true, allowNull: true });
			object.should.property('a').equal(null);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate 8 no options', () => {
		try {
			const object = { a: 8 };
			Validator.validate(object, 'a');
			object.should.property('a').equal(8);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate 8 convet to Boolean', () => {
		try {
			const object = { a: 8 };
			Validator.validate(object, 'a', { convert: Boolean });
			object.should.property('a').equal(true);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate 8 type Number', () => {
		try {
			const object = { a: 8 };
			Validator.validate(object, 'a', { instance: 'Number' });
			object.should.property('a').equal(8);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should not validate 8 type Boolean', () => {
		try {
			const object = { a: 8 };
			Validator.validate(object, 'a', { instance: 'Boolean' });
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.INVALID_TYPE);
		}
	});
	it('Should validate 8 convet to Boolean type Boolean', () => {
		try {
			const object = { a: 8 };
			Validator.validate(object, 'a', { convert: Boolean, instance: 'Boolean' });
			object.should.property('a').equal(true);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should validate 8 verify Number.isInteger', () => {
		try {
			const object = { a: 8 };
			Validator.validate(object, 'a', { verify: Number.isInteger });
			object.should.property('a').equal(8);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
	it('Should not validate 8.73 verify Number.isInteger', () => {
		try {
			const object = { a: 8.73 };
			Validator.validate(object, 'a', { verify: Number.isInteger });
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.INVALID);
		}
	});
	it('Should not validate 8.73 verify Number.isInteger and ceil === round', () => {
		try {
			const object = { a: 8.73 };
			Validator.validate(object, 'a', { verify: [ (a, b) => a && b, Number.isInteger, value => Math.ceil(value) === Math.round(value) ] });
			should.fail(undefined, undefined, 'Should throw');
		} catch (error) {
			error.should.have.property('code').equal(Errors.INVALID);
		}
	});
	it('Should validate 8.73 verify Number.isInteger or ceil === round ', () => {
		try {
			const object = { a: 8.73 };
			Validator.validate(object, 'a', { verify: [ (a, b) => a || b, Number.isInteger, value => Math.ceil(value) === Math.round(value) ] });
			object.should.property('a').equal(8.73);
		} catch (error) {
			should.fail(undefined, undefined, `Should not throw ${error}`);
		}
	});
});
