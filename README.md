# Module Validator

## Usage

### With throw try catch
```javascript
const Validator = require('validator-chain');

function some(p1, p2, p3, error) {
	Validator(p1, 'p1').must.exist().must.be.number().and.not.equal(0).is.lowerThan(256).try();

	Validator(p2, 'p2').exist().string().not.length(0).match(/^[A-Z]{4}$/).try();

	Validator(p3, 'p3').array().not.have.length(6).each((vChild) => {
		vChild.object().keys('address', 'zipcode', 'email', 'age', (vAddress, vZipcode, vEmail, vAge) => {
			vAddress.string(); // Don't call try/resolve on nested chain
			vZipcode.string().match(/^[0-9]{4}$/);
			vEmail.string().match(...);
			vAge.number();
		}); // Don't call try/resolve on nested chain
	}).try();

	// And so on ...
}
```
### With resolve then catch

```javascript
const Validator = require('validator-chain');

function some(parameter) {
	return Validator(parameter, 'parameter').exist().array().not.length(6).each((child) => {
		child.object().keys([  'email', 'age' ], ([ vEmail, vAge ]) => {
			vEmail.string().match(...); // Don't call try/resolve on nested chain
			vAge.number(); // Don't call try/resolve on nested chain
		});
	}).resolve().then(() => {

	}).catch((error) => {

	});

	// And so on ...
}
```

### Express body control

```javascript
const Validator = require('validator-chain');

const logic = {
	and: (a, b) => a && b,
	or: (a, b) => a || b,
}

app.put('/api/catering/catalog/products/:id',
	Validator.controlBody('label', { required: true, allowNull: false, type: 'String' }),
	Validator.controlBody('enabled', { convert: Boolean }),
	Validator.controlBody('quantity', { allowNull: true, verify: Number.isInteger }),
	Validator.controlBody('price', { allowNull: false, verify: [ logic.and,  Number.isInteger, x => x >= 0 ] }),
	Validator.controlBody('reference', { allowNull: true, type: 'String' }),
	(req, res) => {
		// TODO
	});

```

### Future features
Control on neasted property

```javascript
const Validator = require('validator-chain');

const logic = {
	and: (a, b) => a && b,
	or: (a, b) => a || b,
}

app.put('/api/catering/catalog/products/:id',
	Validator.controlBody('address', { allowNull: false, type: 'Object' }),
	Validator.controlBody('address.street', { required: true, type: 'String' }),
	Validator.controlBody('address.zipcode', { required: true, type: 'String', verify: zipcode => /^[0-9]{0,5}$/.test(zipcode) }),
	Validator.controlBody('address.city', { required: true, type: 'String', convert: _.capitalize }),
	Validator.controlBody('address.complement', { type: 'String' }),
	(req, res) => {
		// TODO
	});

```

## References

Coming soon.
See tests while waiting.
